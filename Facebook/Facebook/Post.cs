﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facebook
{
    class Post
    {
        #region Atributos

        private int _ContadorLike;

        private int _ContadorDislike;

        private string _titulo;

        private string _descricao;

        private DateTime _data;
        
        #endregion

        #region propriedades

        public int ContadorLike
        {
            get { return _ContadorLike; }

            set
            {
                if (value >= 0)
                {
                    _ContadorLike = value;
                }
            }
        }

        public int ContadorDislike
        {
            get { return _ContadorDislike; }

            set
            {
                if (value >= 0)
                {
                    _ContadorDislike = value;
                }
            }
        }


        #endregion

    }
}
