﻿namespace Facebook
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.PictureBoxLike = new System.Windows.Forms.PictureBox();
            this.PictureBoxDislike = new System.Windows.Forms.PictureBox();
            this.LabelLike = new System.Windows.Forms.Label();
            this.LabelDislike = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.LabelUtilizador = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.LabelDescricao = new System.Windows.Forms.Label();
            this.LabelData = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxLike)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxDislike)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // PictureBoxLike
            // 
            this.PictureBoxLike.Image = ((System.Drawing.Image)(resources.GetObject("PictureBoxLike.Image")));
            this.PictureBoxLike.Location = new System.Drawing.Point(129, 100);
            this.PictureBoxLike.Name = "PictureBoxLike";
            this.PictureBoxLike.Size = new System.Drawing.Size(36, 24);
            this.PictureBoxLike.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBoxLike.TabIndex = 0;
            this.PictureBoxLike.TabStop = false;
            this.PictureBoxLike.Click += new System.EventHandler(this.PictureBoxLike_Click);
            // 
            // PictureBoxDislike
            // 
            this.PictureBoxDislike.Image = ((System.Drawing.Image)(resources.GetObject("PictureBoxDislike.Image")));
            this.PictureBoxDislike.Location = new System.Drawing.Point(219, 100);
            this.PictureBoxDislike.Name = "PictureBoxDislike";
            this.PictureBoxDislike.Size = new System.Drawing.Size(36, 24);
            this.PictureBoxDislike.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBoxDislike.TabIndex = 1;
            this.PictureBoxDislike.TabStop = false;
            this.PictureBoxDislike.Click += new System.EventHandler(this.PictureBoxDislike_Click);
            // 
            // LabelLike
            // 
            this.LabelLike.AutoSize = true;
            this.LabelLike.Location = new System.Drawing.Point(171, 111);
            this.LabelLike.Name = "LabelLike";
            this.LabelLike.Size = new System.Drawing.Size(27, 13);
            this.LabelLike.TabIndex = 2;
            this.LabelLike.Text = "Like";
            // 
            // LabelDislike
            // 
            this.LabelDislike.AutoSize = true;
            this.LabelDislike.Location = new System.Drawing.Point(261, 111);
            this.LabelDislike.Name = "LabelDislike";
            this.LabelDislike.Size = new System.Drawing.Size(38, 13);
            this.LabelDislike.TabIndex = 3;
            this.LabelDislike.Text = "Dislike";
            this.LabelDislike.Click += new System.EventHandler(this.LabelDislike_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(6, 19);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(104, 69);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // LabelUtilizador
            // 
            this.LabelUtilizador.AutoSize = true;
            this.LabelUtilizador.Location = new System.Drawing.Point(126, 19);
            this.LabelUtilizador.Name = "LabelUtilizador";
            this.LabelUtilizador.Size = new System.Drawing.Size(62, 13);
            this.LabelUtilizador.TabIndex = 5;
            this.LabelUtilizador.Text = "Manuel Luz";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.LabelDislike);
            this.groupBox1.Controls.Add(this.LabelData);
            this.groupBox1.Controls.Add(this.PictureBoxDislike);
            this.groupBox1.Controls.Add(this.LabelLike);
            this.groupBox1.Controls.Add(this.LabelDescricao);
            this.groupBox1.Controls.Add(this.LabelUtilizador);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.PictureBoxLike);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(304, 148);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Post";
            // 
            // LabelDescricao
            // 
            this.LabelDescricao.AutoSize = true;
            this.LabelDescricao.Location = new System.Drawing.Point(129, 46);
            this.LabelDescricao.Name = "LabelDescricao";
            this.LabelDescricao.Size = new System.Drawing.Size(67, 13);
            this.LabelDescricao.TabIndex = 6;
            this.LabelDescricao.Text = "GOOLOOO!!";
            // 
            // LabelData
            // 
            this.LabelData.AutoSize = true;
            this.LabelData.Location = new System.Drawing.Point(129, 74);
            this.LabelData.Name = "LabelData";
            this.LabelData.Size = new System.Drawing.Size(30, 13);
            this.LabelData.TabIndex = 7;
            this.LabelData.Text = "Data";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(328, 172);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Facebook";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxLike)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxDislike)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox PictureBoxLike;
        private System.Windows.Forms.PictureBox PictureBoxDislike;
        private System.Windows.Forms.Label LabelLike;
        private System.Windows.Forms.Label LabelDislike;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label LabelUtilizador;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label LabelData;
        private System.Windows.Forms.Label LabelDescricao;
    }
}

