﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Facebook
{
    public partial class Form1 : Form
    {

        private Post MeuPost;


        public Form1()
        {
            InitializeComponent();
            MeuPost = new Post();

            LabelData.Text = Convert.ToString(DateTime.Now);

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            LabelLike.Text = "0";
            LabelDislike.Text = "0";
        }

        private void PictureBoxLike_Click(object sender, EventArgs e)
        {
            MeuPost.ContadorLike++;
            LabelLike.Text = MeuPost.ContadorLike.ToString();
            
        }

        private void PictureBoxDislike_Click(object sender, EventArgs e)
        {
            MeuPost.ContadorDislike++;
            LabelDislike.Text = MeuPost.ContadorDislike.ToString();
        }


        private void LabelDislike_Click(object sender, EventArgs e)
        {

        }

        
    }
}
